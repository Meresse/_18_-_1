
#include <iostream>

using namespace std;

struct Element
{
	int data;
	Element* next;
};

class Stack
{
	Element* top;

public:

	Stack() : top(nullptr)
	{

	}

	void push(int& inData)
	{
		Element* latest = nullptr;

		latest = new Element;

		latest->data = inData;

		latest->next = top;

		top = latest;

		cout << "push operation is succcessfull" << endl;

		cout << "the pushed element : " << top->data << endl;
	}

	void pop()
	{
		if (top == nullptr)

			cout << "stack is empty" << endl;

		else
		{
			Element* latest = nullptr;

			latest = top;

			top = top->next;

			cout << "pop operation is successfull" << endl;

			cout << "the poped elemet : " << latest->data << endl;

			delete latest;
		}
	}

	void display()
	{
		if (top == nullptr)

			cout << "stack empty" << endl;

		else
		{
			Element* current = top;

			cout << "elements in the stack are :";

			while (current != nullptr)
			{
				cout << " " << current->data;

				current = current->next;
			}

			cout << endl;
		}
	}

	~Stack()
	{
		delete top;
		top = nullptr;
		cout << "Destructor works !" << endl;
	}
};


int main()
{
	string command;
	int data;
	Stack* stack = new Stack;

	do
	{
		cout << "command :";
		cin >> command;

		if (command == "push")
		{
			cout << "data :";
			cin >> data;
			stack->push(data);
		}
		else if (command == "pop")
		{
			stack->pop();

		}
		else if (command == "display")
		{
			stack->display();
		}

	} while (command != "exit");

	delete stack;
	stack = nullptr;
}


